var models  = require('../models');
var express = require('express');
var router  = express.Router();
var bcrypt = require('bcrypt-nodejs');
var utils = require('../utils.js');

router.get('/', function(req, res, next){
    res.send('app');
});

router.get('/callprog/:appid', function(req, res, next){
    res.send(req.params.appid);
});

//Pemeliharaan user
router.get('/prog/sc0001', utils.checkauth, function(req,res,next) {
    models.scp001.findAll({})
    .then(function(users){

        models.scp012.findAll({
             order: [
                 ['sdaplnum', 'ASC']
            ]
        })
        .then(function(menus){
            var msg = req.flash('info');
            res.render('register', {
                messages: msg[0],
                userdata: req.session.userdata,
                usermenus: req.session.usermenus,
                users: users,
                menus: menus
            });
        });

    });
});

router.post('/prog/ksp1', utils.checkauth, function(req,res,next) {
    res.json({
        data: req.body
    });
});

// Pemeliharaan sub-menu level1
router.get('/prog/sc0020', utils.checkauth, function(req,res,next) { var msg = req.flash('info');
    models.scp012.findAll({
    
    }).then(function(menus){
        res.render('menulevel1.jade', {
            messages: msg[0],
            userdata: req.session.userdata,
            usermenus: req.session.usermenus,
            menus: menus,
        });
    });
});

router.post('/prog/sc0020', utils.checkauth, function(req,res,next) {
    var msgstatus='';
    if (!req.body.sdapldsc || req.body.sdapldsc.length===0) {
        msgstatus = 'Deskripsi tidak boleh kosong';
    }
    if (!req.body.action) {
        msgstatus += 'Invalid request';
    }
    if (!msgstatus) {
        if (req.body.action=="create"){
            var newmenu = {
                sdapldsc: req.body.sdapldsc
            };
            models.scp012.create(newmenu)
            .then(function(menu) {
                if (menu) {
                    req.flash('info', 'Menu created');
                } else {
                    req.flash('info', 'Menu failed to be created');
                }
                res.redirect('/app/prog/sc0020');
            });
        } else {
            var newdata = {
                sdaplnum: req.body.sdaplnum,
                sdapldsc: req.body.sdapldsc
            };
            models.scp012.update(
                newdata, {
                    where: {
                        sdaplnum: newdata.sdaplnum
                    }
                })
            .then(function(resupdate){
                req.flash('info', 'Menu edited!');
                res.redirect('/app/prog/sc0020');
            });
        }
    } else {
        req.flash('info', msgstatus);
        res.redirect('/app/prog/sc0020');
    }
});

router.get('/prog/sc0020/rm/:sdaplnum', utils.checkauth, function(req, res, next){
    models.scp012.destroy({
        where: {
            sdaplnum: req.params.sdaplnum
        }
    }).then(function(results){
        req.flash('info', 'Menu deleted');
        res.redirect('/app/prog/sc0020');
    });
});
 
// Pemeliharaan sub-menu level2
router.get('/prog/sc0021', utils.checkauth, function(req,res,next) {
    models.scp012.findAll({
        include: [{
            model: models.scp011,
         }],
         order: [
             ['sdaplnum', 'ASC']
        ]
    }).then(function(menus){
        res.render('submenulevel2.jade', {
            userdata: req.session.userdata,
            usermenus: req.session.usermenus,
            menus: menus,
        });
    });
});

router.get('/prog/ea', function(req,res,next) {
    var tet = utils.stringParser(2, "aabbcc");
    res.json({
        data: tet
    });
});

// Parameter Aplikasi
router.get('/prog/sp0001', utils.checkauth, function(req,res,next) {
    models.spp001.findOne({
        where: {
            xkunci: '001'
        }
    }).then(function(viewdata){
        //var dates = utils.stringParser(10, viewdata.xdata);
        res.render('spp001.jade', {
            userdata: req.session.userdata,
            usermenus: req.session.usermenus,
            dates: viewdata.xdata
        });
    });
});

// Parameter Identitas
router.get('/prog/sp0002', utils.checkauth, function(req,res,next) {
    models.spp001.findOne({
        where: {
            xkunci: '000'
        }
    }).then(function(viewdata){
        res.render('sp0002.jade', {
            userdata: req.session.userdata,
            usermenus: req.session.usermenus,
            viewdata: (viewdata) ? viewdata.xdata : ""
        });
    });
});

// Parameter Bunga Tabungan
router.get('/prog/sp0006', utils.checkauth, function(req,res,next) {
    res.render('sp0006.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Rencana Biaya Layanan
router.get('/prog/sp0007', utils.checkauth, function(req,res,next) {
    res.render('sp0007.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Pemeliharaan Kode Produk Tabungan
router.get('/prog/sp0008', utils.checkauth, function(req,res,next) {
    res.render('sp0008.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Kode Transaksi
router.get('/prog/sp0009', utils.checkauth, function(req,res,next) {
    res.render('sp0009.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Data Kolektor
router.get('/prog/sp0010', utils.checkauth, function(req,res,next) {
    res.render('sp0010.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Kode Pejabat Bank
router.get('/prog/sp0011', utils.checkauth, function(req,res,next) {
    res.render('sp0011.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Data Wilayah
router.get('/prog/sp0012', utils.checkauth, function(req,res,next) {
    res.render('sp0012.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Pejabat Penandatanganan Lap
router.get('/prog/sp0013', utils.checkauth, function(req,res,next) {
    res.render('sp0013.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Produk Pinjaman
router.get('/prog/sp0014', utils.checkauth, function(req,res,next) {
    res.render('sp0014.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Sandi BI Pinjaman
router.get('/prog/sp0016', utils.checkauth, function(req,res,next) {
    res.render('sp0016.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Tabel Pinjaman
router.get('/prog/sp0017', utils.checkauth, function(req,res,next) {
    res.render('sp0017.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Kode Cabang
router.get('/prog/sp0018', utils.checkauth, function(req,res,next) {
    res.render('sp0018.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});

// Parameter Pemilik/Pengurus BPR
router.get('/prog/sp0019', utils.checkauth, function(req,res,next) {
    res.render('sp0019.jade', {
        userdata: req.session.userdata,
        usermenus: req.session.usermenus,
    });
});


module.exports = router;
