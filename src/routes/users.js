var models  = require('../models');
var express = require('express');
var router  = express.Router();

router.get('/:user_id/tasks', function(req, res, next){
    models.Task.findAll({
       userid: req.params.user_id 
    })
    .then(function(results){
        res.json({
            data: results
        });
    });
});

router.post('/create', function(req, res) {
  models.User.create({
    username: req.body.username
  }).then(function() {
    res.redirect('/');
  });
});

router.get('/:user_id/destroy', function(req, res) {
  models.User.destroy({
    where: {
      id: req.params.user_id
    }
  }).then(function() {
    res.redirect('/');
  });
});

router.post('/:user_id/tasks/create', function (req, res) {
  models.Task.create({
    title: req.body.title,
    UserId: req.params.user_id
  }).then(function() {
    res.redirect('/');
  });
});

router.get('/:user_id/tasks/:task_id/destroy', function (req, res) {
  models.Task.destroy({
    where: {
      id: req.params.task_id
    }
  }).then(function() {
    res.redirect('/');
  });
});

//=============== DEVELOP METHOD
router.get('/json', function(req,res,next) {
    models.scp001.findAll({})
    .then(function(users){
        res.json({
            data: users
        });
    });
});


module.exports = router;
