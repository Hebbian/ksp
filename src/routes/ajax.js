var models  = require('../models');
var express = require('express');
var router  = express.Router();
var bcrypt = require('bcrypt-nodejs');
var utils = require('../utils.js');
var settings = require('../settings.js');
var _ = require('underscore');
var moment = require('moment');

var createHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};
var isValidPassword = function(user, password) {
    return bcrypt.compareSync(password, user.scpass);
};

router.get('/', function(req, res, next){
    res.send('ajax');
});

router.get('/scp002/:sauser/:saappl', function(req, res, next){
   models.scp002.find({
       where: {
           sauser: req.params.sauser,
           saappl: req.params.saappl
       }
   }).then(function(results){
       res.json({
           data: results
       });
   }); 
});

router.get('/scp001/:scuser', function(req, res, next){
   models.scp001.find({
       where: {
           scuser: req.params.scuser,
       }
   }).then(function(results){
       res.json({
           data: results
       });
   }); 
});

router.get('/scp011/:sdapldsc', function(req, res, next){
    models.scp012.findOne({
        where: {
            sdapldsc: req.params.sdapldsc
        },
        include: [{
            model: models.scp011
         }]
    }).then(function(results){
        res.json({
            data: results
        });
    });
});

router.post('/dscp011', function(req, res, next){
    models.scp011.destroy({
        where: {
            scpgm: req.body.scpgm
        }
    })
    .then(function(results){
        res.json({
            data: results
        });
    });
});

router.post('/uscp011', function(req, res, next){
    if (!req.session.userdata) {
        res.send(401);
    } else {
        var msgstatus='';
        if (!req.body.scpgm || !req.body.scodsc) {
            msgstatus += 'Invalid data';
        }
        if (!msgstatus) {
            models.scp011.update(
                req.body,
                {
                    where: {
                       id: req.body.id 
                    }
                })
            .then(function(resupdate){
                res.json({
                    data: resupdate
                });
            });
        } else {
            req.json({
                status: false,
                message: msgstatus,
                data: ''
            });
        }
    }
});

router.post('/cscp001', function(req, res, next){
    if (!req.session.userdata) {
        res.send(401);
    } else {
        var msgstatus='';
        if (!req.body.scuser) {
            msgstatus += 'User ID tidak boleh kosong';
        }
        if (!req.body.scunme) {
            msgstatus += 'Nama tidak boleh kosong';
        }
        if (!msgstatus) {
            console.log(req.body);
            var newscp001 = {
                scuser: req.body.scuser,
                scunme: req.body.scunme,
                scpass: createHash(settings.DEFAULT_PASSWORD),
                scauth: req.body.scauth,
                scstat: req.body.scstat,
                scdrdat: req.body.scdrdat,
                scdrmod: req.body.scdrmod,
                scdrpgm: req.body.scdrpgm,
                scdrrpt: req.body.scdrrpt,
                scexp: req.body.scexp,
                scfreq: req.body.scfreq,
                scnxdt: req.body.scnxdt,
                sclsdt: req.body.sclsdt,
                //sclast: req.body.sclast,
                //scopdt: req.body.scopdt,
                scdepn: req.body.scdepn,
                scspvr: req.body.scspvr,
                scsgon: req.body.scsgon,
                scid: req.body.scid,
                scpgm: req.body.scpgm,
                scact: req.body.scact,
                sccntr: req.body.sccntr,
                scnwil: req.body.scnwil,
            };
            newscp001.sclast = moment(req.body.sclast, "DD/MM/YYYY").format('YYYY-MM-DD');
            newscp001.scopdt = moment(req.body.scopdt, "DD/MM/YYYY").format('YYYY-MM-DD');

            console.log(newscp001);

            models.scp001.findOrCreate({
                where: {
                    scuser: newscp001.scuser
                }, 
                defaults: newscp001
            })
            .spread(function(user, created){


                if (!created) {
                    models.scp001.update(
                        newscp001, {
                            where: {
                                scuser: user.scuser
                            }
                        })
                    .then(function(resupdate){
                    
                    });
                }

                console.log(req.body.menudatas);

                if (req.body.menudatas.length > 0) {
                    _.each(req.body.menudatas, function(v,k){
                        if (v.sainqu!==0 || v.samain!==0 || v.samoni!==0 || v.saoper!==0) {
                            var newscp002 = {
                                sauser: user.scuser,
                                saappl: v.saappl,
                                sainqu: v.sainqu,
                                samain: v.samain,
                                samoni: v.samoni,
                                saoper: v.saoper,
                                sacrlm: v.sacrlm,
                                sadrlm: v.sadrlm,
                                saover: v.saover
                            };
                            console.log(newscp002);

                            models.scp002.destroy({
                                where: {
                                    sauser: newscp002.sauser,
                                    saappl: newscp002.saappl
                                }
                            }).then(function(results){
                                models.scp002.create(newscp002)
                                .then(function(rescreate){
                                    console.log(rescreate);
                                });
                            });
                        }
                    });
                }
                req.flash('info','User altered!');
                res.json({
                    created: created,
                    data: user
                });
            });
            
        } else {
            res.json({
                status: false,
                message: msgstatus,
                data: ''
            });
        }
    }
});

router.post('/cscp011', function(req, res, next){
    if (!req.session.userdata) {
        res.send(401);
    } else {
        var msgstatus='';
        if (!req.body.scodsc || !req.body.scp012Sdaplnum || !req.body.scpgm) {
            msgstatus += 'Invalid Data';
        }
        if (!msgstatus) {
            var newscp011 = {
                scodsc: req.body.scodsc,
                scp012Sdaplnum: req.body.scp012Sdaplnum,
                scpgm: req.body.scpgm,
                scttyp: req.body.scttyp,
                sctlvl: req.body.sctlvl
            };
            models.scp011.create(newscp011)
            .then(function(results){
                res.json({
                    data: results
                });
            });
        }
    }
});

router.get('/gspp001/:xkunci', function(req, res, next){
    if (!req.session.userdata) {
        res.send(401);
    } else {
        models.spp001.findOne({
            where: {
                xkunci: req.params.xkunci
            }
        }).then(function(results){
            res.json({
                data: results
            });
        });
    }
});

router.get('/rspp001/:xkunci', function(req, res, next){
    if (!req.session.userdata) {
        res.send(401);
    } else {
        models.spp001.findOne({
            where: {
                xkunci: req.params.xkunci
            }
        }).then(function(results){
            res.json({
                data: results
            });
        });
    }
});

router.post('/cspp001', function(req, res, next) {
    if (!req.session.userdata) {
        res.send(401);
    } else {
        var postdata = req.body.postdata;
        _.each(postdata, function(v,k){
            var newdata = {
                xkunci: v.xkunci,
                xdata: v.xdata
            };
            models.spp001.findOrCreate({
                where: {
                    xkunci: newdata.xkunci
                }, 
                defaults: newdata
            })
            .spread(function(paramdata, created){
                console.log(created);
                if (!created) {
                    models.spp001.update(
                        {xdata: newdata.xdata}, {
                            where: {
                                xkunci: newdata.xkunci
                            }
                        }
                    ).then(function(results){
                        if (k==postdata.length-1) {
                            res.json({
                                data: results
                            });
                        }
                    });
                } else {
                    if (k==postdata.length-1) {
                        res.json({
                            data: true
                        });
                    }
                }
            });
        
        });
    }
});

router.get('/rspp001B/:xkunci', function(req, res, next) {
    if (!req.session.userdata) {
        res.send(401);
    } else {
        models.spp001.findAll({
            where: {
                xkunci: {
                    $like: req.params.xkunci + "%"
                }
            }
            //where: ["xkunci LIKE ? ", "201%"]
        }).then(function(results){
            res.json({
                data: results,
            });
        });
    }
});

router.post('/dspp001', function(req, res, next){
    models.spp001.destroy({
        where: {
            xkunci: req.body.xkunci
        }
    })
    .then(function(results){
        res.json({
            data: results
        });
    });
});


module.exports = router;
