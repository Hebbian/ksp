var models  = require('../models');
var express = require('express');
var router  = express.Router();
var bcrypt = require('bcrypt-nodejs');
var settings = require('../settings.js');
var _ = require('underscore');
var fs = require('fs');

var createHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
};
var isValidPassword = function(user, password) {
    return bcrypt.compareSync(password, user.scpass);
};

router.get('/', function(req, res) {
    if (!req.session.userdata) {
        res.redirect('/auth/login');
    } else {
        res.render('index', {
          userdata: req.session.userdata,
          usermenus: req.session.usermenus
        });
    }
});

router.get('/json', function(req, res, next){
    models.User.findAll({
        include: [{
            model: models.Task,
            attributes: [ 'id', 'title' ]
         }]
    }).then(function(users){
        res.json({
            data: users
        });
    });
});

router.get('/task/json', function(req, res, next){
    models.Task.findAll({
    }).then(function(users){
        res.json({
            data: users
        });
    });
});

router.get('/auth/login', function(req, res, next){
    if (req.session.userdata) {
        res.redirect('/');
    } else {
        var msg = req.flash('info');
        res.render('login', {
            messages: msg[0]
        });
    }
});

router.post('/auth/login', function(req, res, next){
    msgstatus = '';
    if (!req.body.scuser) {
        msgstatus += "Invalid data";
    }
    if (!req.body.scpass) {
        msgstatus += "Invalid data";
    }
    if (!msgstatus) {
        models.scp001.findOne({
            where: {
                scuser: req.body.scuser,
            }
        })
        .then(function(results){
            if (!results) {
                req.flash('info', 'user not found');
                res.redirect('/auth/login');
            } else if (!isValidPassword(results, req.body.scpass)) {
                res.send('invalid password...');
            } else {


                models.scp012.findAll({
                    include: [{
                        model: models.scp011,
                     }],
                     order: [
                         ['sdaplnum', 'ASC']
                    ]
                }).then(function(menus){
                    req.session.userdata = results; 

                    models.scp002.findAll({
                        where: {
                            sauser: results.scuser
                        }
                    })
                    .then(function(resscp002) {
                        var filteredmenus = [];
                        _.each(menus, function(v,k) {
                            var submenu=[];
                            if (v.scp011s.length > 0) {
                                _.each(v.scp011s, function(p,q) {
                                    _.each(resscp002, function(r,s) {
                                        if (r.saappl == v.sdaplnum) {
                                            if (p.scttyp == 1) { 
                                                if (r.sainqu >= p.sctlvl) {
                                                    submenu.push(p);
                                                }
                                            } else if (p.scttyp == 2) { 
                                                if (r.samain >= p.sctlvl) {
                                                    submenu.push(p);
                                                }
                                            } else if (p.scttyp == 3) { 
                                                if (r.samoni >= p.sctlvl) {
                                                    submenu.push(p);
                                                }
                                            } else if (p.scttyp == 4) { 
                                                if (r.saoper >= p.sctlvl) {
                                                    submenu.push(p);
                                                }
                                            } 
                                        }
                                    });
                                });

                                var newvar = {
                                    sdaplnum: v.sdaplnum,
                                    sdapldsc: v.sdapldsc,
                                    scp011s: submenu
                                };
                                filteredmenus.push(newvar);
                            }
                        });
                        req.session.usermenus = filteredmenus;
                        if (req.query.callback) {
                            res.redirect(req.query.callback);
                        } else {
                            res.redirect('/');
                        }
                    });
                });
            }
        });
    } else {
       res.redirect('/auth/login'); 
    }
});

router.get('/auth/register', function(req, res, next){
    res.render('register', {
    
    });
});


router.post('/auth/register', function(req, res, next){
    msgstatus = '';
    if (!req.body.scuser) {
        msgstatus += "Invalid data";
    }
    if (!req.body.scunme) {
        msgstatus += "Invalid data";
    }
    if (!req.body.scpass) {
        msgstatus += "Invalid data";
    }
    if (!msgstatus) {
        models.scp001.create({
            scuser: req.body.scuser,
            scunme: req.body.scunme,
            scpass: createHash(req.body.scpass),
        }).then(function(results){
            res.json({
                results: results
            });
        });
    } else {
       res.redirect('/auth/register'); 
    }
});

router.get('/auth/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/auth/login');
});


/* ========= DEVELOPER METHODS ======== */
router.get('/ewo', function(req, res, next){
    if (!req.session.userdata) {
        res.json({
            data: 'ewo'
        });
    } else {
        var ewo = createHash(settings.DEFAULT_PASSWORD);
        res.json({
            data: ewo
        });
    
    }
});

router.get('/menus', function(req, res, next){
    models.scp012.findAll({
        include: [{
            model: models.scp011,
            //attributes: [ 'id', 'title' ]
         }],
         order: [
             ['sdaplnum', 'ASC']
        ]
    }).then(function(menus){
        res.json({
            data: menus
        });
    });
});

router.get('/install', function(req, res, next){
    models.scp001.findOne({
        where: {
            scuser: 'MANAGER1'
        }
    }).then(function(resfind){
        if (!resfind) {
            var sql = fs.readFileSync('install.sql').toString();
            models.sequelize.query(sql)
            .spread(function(results, metadata){
                res.send('Default DB Installed');
            });
        } else {
            res.send('DB Instalation failed, data already exists!');
        }
    });
});

module.exports = router;
