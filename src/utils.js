var models  = require('./models');

var utils = {
    checkauth: function checkauth(req, res, next) {
        if (!req.session.userdata) {
            var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
            res.redirect('/auth/login?callback=' + fullUrl);
        } else {
            return next();
        }
    },
    stringParser: function stringParser(count, string) {
        console.log('string.length: ' + string.length);
        var ret=[], tmp='', counter=0;
        for (var i=0; i< string.length; i++) {
            //tmp[counter]=string[i];
            tmp+=string[i];
            console.log('counter: '+ counter +' | i: ' + i + ' | elem: ' + string[i]);
            if (counter==(count-1)) {
                ret.push(tmp);
                tmp='';
                counter=0;
            } else {
                counter++;
            }
        } 
        console.log(ret);
        return ret;
    },
    makeIndonesianDay: function makeIndonesianDay(param) {
        var dayname = param.toLowerCase();
        if (param==="sunday") {
            return "minggu";
        } else if (param==="monday") {
            return "senin";
        } else if (param==="tuesday") {
            return "selasa";
        } else if (param==="wednesday") {
            return "rabu";
        } else if (param==="thursday") {
            return "kamis";
        } else if (param==="friday") {
            return "jumat";
        } else if (param==="wednesday") {
            return "sabtu";
        }
    },
};
module.exports = utils;
