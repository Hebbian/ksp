module.exports = function(sequelize, DataTypes) {
  var model  = sequelize.define("scp011", {
    sctran: DataTypes.STRING(10),
    scpgm: DataTypes.STRING(8),
    scttyp: { type: DataTypes.INTEGER(1), defaultValue: 0 },
    sctlvl: { type: DataTypes.INTEGER(1), defaultValue: 0 },
    sctovr: DataTypes.ENUM('Y', 'N'),
    sctovl: { type: DataTypes.INTEGER(1), defaultValue: 0 },
    scdual: DataTypes.ENUM('Y', 'N'),
    scdlvl: { type: DataTypes.INTEGER(1), defaultValue: 0 },
    scodsc: DataTypes.STRING(60),
    scuse: DataTypes.ENUM('Y', 'N'),
  },{
    timestamps: false,                           
    classMethods: {
      associate: function(models) {
        model.belongsTo(models.scp012, {
          onDelete: "CASCADE",
          foreignKey: {
            allowNull: false
          }
        });
      }
    }
  });
  return model;
};
