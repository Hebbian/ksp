module.exports = function(sequelize, DataTypes) {
  var model  = sequelize.define("scp001", {
    scuser: { type: DataTypes.STRING(10), primaryKey: true, unique: true },
    scunme: DataTypes.STRING(20),
    scpass: DataTypes.STRING(128),
    scauth: DataTypes.STRING(1),
    scstat: DataTypes.BOOLEAN,
    scdrdat: DataTypes.STRING(35),
    scdrmod: DataTypes.STRING(35),
    scdrpgm: DataTypes.STRING(35),
    scdrrpt: DataTypes.STRING(35),
    scexp: DataTypes.STRING(35),
    scfreq: DataTypes.INTEGER,
    scnxdt: DataTypes.DATE,
    sclsdt: DataTypes.DATE,
    sclast: DataTypes.DATE,
    scopdt: { type:DataTypes.DATE, defaultValue:DataTypes.NOW },
    scdepn: DataTypes.INTEGER,
    scspvr: DataTypes.STRING(1),
    scsgon: DataTypes.ENUM('Y', 'N'),
    scid: DataTypes.STRING(15),
    scpgm: DataTypes.STRING(10),
    scact: DataTypes.STRING(10),
    sccntr: DataTypes.STRING(7),
    scnwil: DataTypes.STRING(2),
  },{
    timestamps: false,                           
  });
  return model;
};
