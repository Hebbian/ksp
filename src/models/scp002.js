module.exports = function(sequelize, DataTypes) {
  var model  = sequelize.define("scp002", {
    //scp002id: { type: DataTypes.INTEGER, primaryKey: true },
    sauser: { type: DataTypes.STRING(10) },
    saappl: DataTypes.STRING(2),
    sainqu: DataTypes.INTEGER,
    samain: DataTypes.INTEGER,
    samoni: DataTypes.INTEGER,
    saoper: DataTypes.INTEGER,
    sadrlm: DataTypes.BIGINT,
    sacrlm: DataTypes.BIGINT,
    saover: DataTypes.BOOLEAN,
  },{
    timestamps: false,                           
  });

  return model;
};
