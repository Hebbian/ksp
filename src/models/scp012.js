module.exports = function(sequelize, DataTypes) {
  var model  = sequelize.define("scp012", {
    //sdaplnum: { type: DataTypes.STRING(2), primaryKey: true },
    sdaplnum: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    sdapldsc: DataTypes.STRING(150),
  },{
    timestamps: false,                           
    classMethods: {
        associate: function(models) {
            model.hasMany(models.scp011)
        }
    }
  });
  return model;
};
