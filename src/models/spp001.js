module.exports = function(sequelize, DataTypes) {
  var model  = sequelize.define("spp001", {
    xkunci: DataTypes.TEXT,
    xdata: DataTypes.TEXT,
  },{
    timestamps: false,                           
  });
  return model;
};
