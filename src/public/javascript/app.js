(function() {

    var app = angular.module('ksp', ['ui.slider', 'ui.bootstrap.modal', 'ui.utils.masks', 'ui.mask', 'ngMask', 'loadingButton', 'cgNotify', 'ui-notification']);
    app.run(function($rootScope){
        $rootScope.onlyNumbers = /^\d+$/;
    });

}());
