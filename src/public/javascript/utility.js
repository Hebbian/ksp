(function(app) {

    app.factory('Utility', function($rootScope){
        return {
            normalizeCharNum: function(charnum, charstr) {
                var tmp = (charstr===undefined) ? '' : charstr;
                for (var i=0;i<charnum;i++) {
                    if (!tmp[i]) {
                        tmp=tmp+" ";
                    }
                }
                return tmp;
            },
            parseStringToModels: function(scope, models, numarray, stringdata) {
                var streamchar = "";
                var counter=0;
                var elem=0;
                var elemval=numarray[elem];
                for (var i=0; i< stringdata.length; i++) {
                    streamchar = streamchar + stringdata[i];
                    counter = i+1; 
                    if (counter==elemval) {
                        scope[models[elem]] = streamchar;
                        elem+=1;
                        elemval+=numarray[elem];
                        streamchar="";
                    }
                }
            
            }
        }
    });

}(angular.module('ksp')));
