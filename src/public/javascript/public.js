(function(app) {

    app.controller('otoritasCtrl', function($scope, $http, $window, $rootScope, Notification){
        $scope.isLoading = false;
        $scope.formdata={};
        $scope.formdata.scopdt = moment().format('DD/MM/YYYY');
        $scope.menudatas=[];
        $scope.selectedMenudata={};
        $scope.selblock=1;
        $scope.toggleBlock = function() {
            if ($scope.selblock==1 && !$scope.formdata.scuser) {
                alert('user ID harus terisi');
            } else {
                $scope.selblock = ($scope.selblock==1) ? 2 : 1;
            }
        }
        function defaultsliderval(param) {
            return {
                saappl: param,
                sainqu: 0,
                samain: 0,
                samoni: 0,
                saoper: 0,
                sacrlm: 0,
                sadrlm: 0,
                saover: 'N'
            }
        }
        $scope.menuClicked = function(param) {
            if ($scope.formdata.scuser) {
                var exists = false;
                angular.forEach($scope.menudatas, function(v,k){
                    if (v.saappl == param) {
                        $scope.selectedMenudata = v;
                        exists = true;
                    }
                });
                if (!exists) {
                    $http.get('/ajax/scp002/'+$scope.formdata.scuser+'/'+param)
                    .success(function(response){
                        console.log(response);
                        if (response.data != null) {
                            $scope.selectedMenudata = response.data;
                            $scope.menudatas.push(response.data);
                        } else {
                            var def = defaultsliderval(param);
                            $scope.selectedMenudata = def;
                            $scope.menudatas.push(def);
                        }
                    });
                }
            }
        }
        $scope.kspsubmit = function(isValid) {
            if (!isValid) {
                alert('Invalid Data');
            } else {
               $scope.formdata.menudatas = $scope.menudatas;
               console.log($scope.formdata);
                $scope.isLoading = true;
               $http({
                   url: '/ajax/cscp001',
                   method: 'POST',
                   data: angular.toJson($scope.formdata),
                   header: {'Content-Type': 'application/json'},
               }).success(function(data, status, headers, config){
                   console.log(data);
                   if (data.data) {
                       //$window.location.href="/app/prog/sc0001";
                       $scope.isLoading=false;
                        Notification("Data berhasil disimpan!");
                   }
               }).error(function(data, status, headers, config){
                   console.log('ERROR');
               });
            }
        }
        $scope.userClicked = function(param) {
            $http.get('/ajax/scp001/'+param)
            .success(function(response){
                console.log(response);
                if (response.data != null) {
                    $scope.formdata = response.data;
                    if ($scope.formdata.scopdt!==null) {
                        $scope.formdata.scopdt = moment(response.data.scopdt).format('DD/MM/YYYY');
                    } else {
                        $scope.formdata.scopdt = moment().format('DD/MM/YYYY');
                    }

                    $scope.menudatas=[];
                    $scope.selectedMenudata={};
                } else {

                }
            });
        }
        $scope.$watch('formdata.scstat', function(param){
            console.log(param);
        });

        $scope.$watch('formdata.scfreq', function(param){
            if (typeof param !== "undefined") {
               var a = $(".daritanggal").data("DateTimePicker").date(); 
               var c = moment($scope.formdata.scopdt, "DD/MM/YYYY");
               var dd = moment(c).add($scope.formdata.scfreq,"d");
               $scope.formdata.sclast = dd.date() + '/' + (dd.month()+1) + '/' + dd.year();
            }
        });
        $scope.$watch('formdata.sclast', function(param){
        });
        $(".sampaidengan").on("dp.change", function(e){
            var a = e.date.format("DD/MM/YYYY");
            var b = moment(moment(a).diff(moment())).format("DD");
            var c = moment($scope.formdata.scopdt, "DD/MM/YYYY");
            $scope.formdata.scfreq = e.date.diff(c, "days");
            $scope.$apply();
        });
    });

    $(document).ready(function(){
        $(".datetimepicker").datetimepicker({
            format: "DD/MM/YYYY"
        });
        $(".datetimepickerdefault").datetimepicker({
            defaultDate: new Date(),
            format: "DD/MM/YYYY"
        });
        $(document).on('click', '.addmenusubmitbtn', function(e){
            $('.addmenuform').submit();
        });
        $(".datepicker").datepicker();
    });

}(angular.module('ksp')));
