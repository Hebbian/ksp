(function(app) {
    app.controller('menuLevel1Ctrl', function($scope, $http, $window){
        $scope.isLoading=false;
        $scope.selectedMenu = {};
        $scope.selectedMenuIndex = -1;
        $scope.modalTitle = "";
        $scope.modalAction = "";
        $scope.menuClicked = function(param, param1, param2) {
            $scope.selectedMenuIndex = param;
            $scope.selectedMenu = {sdaplnum: param1, sdapldsc: param2};
        }
        $scope.hapusClicked = function(param){
            if ($scope.selectedMenu.sdaplnum) {
                var conf = confirm("Apakah Anda yakin akan menghapus menu "+$scope.selectedMenu.sdapldsc+"?");
                if (conf){
                    $window.location.href='/app/prog/sc0020/rm/'+$scope.selectedMenu.sdaplnum;
                }
            }
        }
        $scope.editClicked = function() {
            if ($scope.selectedMenu.sdaplnum) {
                $scope.modalTitle = "Edit Menu";
                $scope.menuModal = true;
                $scope.modalAction = "update";
            }
        }
        $scope.cancel = function() {
            $scope.menuModal = false;
        }
        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Menu";
            $scope.selectedMenu = {};
            $scope.selectedMenuIndex = -1;
            $scope.menuModal = true;
            $scope.modalAction = "create";
        }
    });
}(angular.module('ksp')));
