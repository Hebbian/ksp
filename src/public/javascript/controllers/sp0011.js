(function(app) {
    app.controller('sp0011Ctrl', function($scope, $http, $window, $rootScope, Notification, Utility){
        $scope.isLoading=false;
        var arraynum = [2,30,35,10];
        var prefixdb = "401";
        $scope.selectedItemIndex=-1;
        $scope.itemList=[];
        $scope.itemObjectList=[];
        $scope.selectedItem={};
        $scope.selectedItemEdited={};

        function fetchItemList() {
            $scope.itemList=[];
            $scope.itemObjectList=[];
            $scope.selectedItem={};
            $scope.selectedItemEdited={};
            $http.get('/ajax/rspp001B/' + prefixdb)
            .success(function(response){
                angular.forEach(response.data, function(v,k){
                    var streamchar='';
                    var tmp = [];
                    var counter=0;
                    var elem=0;
                    var elemval=arraynum[elem];
                    for (var i=0;i<v.xdata.length;i++) {
                        streamchar=streamchar+v.xdata[i];
                        counter = i+1; 
                        if (counter==elemval) {
                            tmp.push(streamchar);
                            elem+=1;
                            elemval+=arraynum[elem];
                            streamchar="";
                        }
                    }
                    $scope.itemList.push(tmp);
                    $scope.itemObjectList.push(v);
                });
            });
        }

        fetchItemList();

        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Data";
            $scope.menuModal = true;
            $scope.selectedItem={};
            $scope.selectedItemIndex=-1;
            resetAllModels();
        }
        $scope.cancel = function() {
            $scope.menuModal = false;
        }
        function resetAllModels(){
            for (var i=0;i<arraynum.length;i++) {
                $scope["model"+(i+1)] = '';
            }
        }
        function applyAllModels(param){
            var q=[];
            for (var i=0;i<arraynum.length;i++) {
                q.push("model"+(i+1));
            }
            Utility.parseStringToModels($scope, q, arraynum, param);
        }
        $scope.itemClicked = function(param) {
            $scope.selectedItemIndex = param;
            $scope.selectedItem = $scope.itemObjectList[param];
            $scope.selectedItemEdited = JSON.parse(JSON.stringify($scope.selectedItem));
            applyAllModels($scope.selectedItemEdited.xdata);
        }
        $scope.editClicked = function() {
            if ($scope.selectedItemIndex==-1) {
                return;
            }
            $scope.modalTitle = "Edit Data";
            $scope.selectedItem = {};
            $scope.selectedItemIndex= -1;
            $scope.menuModal = true;
        }
        $scope.simpanButtonClicked = function() {
            var finstr='';
            for (var i=0; i<arraynum.length; i++){
                var modelname = "model"+(i+1);
                var normalize = Utility.normalizeCharNum(arraynum[i], $scope[modelname]);
                finstr = finstr+normalize;
            }
            console.log(finstr);
            var data1 = {xkunci: prefixdb + $scope["model1"], xdata:finstr};
            var postdata = {postdata:[data1]};
            $scope.isLoading=true;
            $http({ url: '/ajax/cspp001',
                  method: 'POST',
                  data: angular.toJson(postdata),
                  header: {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
               console.log(data);
               $scope.isLoading=false;
               if (data.data) {
                   $scope.menuModal = false;
                   $scope.selectedItemIndex=-1;
                   fetchItemList();
                   Notification("Data berhasil disimpan!"); 
               }
            }).error(function(data, status, headers, config){
                $scope.isLoading=false;
                console.log('ERROR');
            });
        }
        $scope.hapusClicked = function() {
            if ($scope.selectedItemIndex !=-1) {
                var selobj = $scope.itemList[$scope.selectedItemIndex ];
                var conf = confirm("Apakah Anda akan menghapus data " + selobj[2] + "?");
                var xkunci = prefixdb + selobj[0];
                if (conf) {
                   $scope.isLoading=true;
                   $http({
                       url: '/ajax/dspp001',
                       method: 'POST',
                       data: angular.toJson({xkunci: xkunci}),
                       header: {'Content-Type': 'application/json'},
                   }).success(function(data, status, headers, config){
                       console.log(data);
                       $scope.isLoading=false;
                       resetAllModels();
                       fetchItemList();
                       $scope.selectedItemIndex =-1;
                       Notification("Data berhasil dihapus!");
                   }).error(function(data, status, headers, config){
                       $scope.isLoading=false;
                       console.log('ERROR');
                   });
                }
            }
        }
    });
}(angular.module('ksp')));
