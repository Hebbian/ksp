(function(app) {
    app.controller('sp0006Ctrl', function($scope, $http, $window, $rootScope, Notification, Utility){
        $scope.selectedBungaIndex=-1;
        $scope.isLoading = false;
        $scope.bungaList = [];
        $scope.bungaObjectList = [];
        $scope.selectedBunga = {};
        $scope.selectedBungaEdited = {};

        function fetchBungaList() {
            $scope.bungaList = [];
            $scope.bungaObjectList = [];
            $scope.selectedBunga = {};
            $scope.selectedBungaEdited = {};
            $http.get('/ajax/rspp001B/201')
            .success(function(response){
                angular.forEach(response.data, function(v,k){
                    var streamchar='';
                    var tmp = {};
                    for (var i=0;i<32;i++) {
                        streamchar=streamchar+v.xdata[i];
                        if (i==1) {
                            tmp.kode = streamchar;
                            streamchar='';
                        }
                    }
                    tmp.namabunga = streamchar;
                    $scope.bungaList.push(tmp);
                    $scope.bungaObjectList.push(v);
                })
            });
        }

        fetchBungaList();

        function resetAllModels() {
            $scope.koderencanabunga = '';
            $scope.namarencanabunga = '';
            $scope.perhitunganbunga = '';
            $scope.tandapajak = '';
            $scope.persentasepajak = '';
            $scope.perhitunganpajak = '';
            $scope.minimalpajak = '';
            $scope.minimalmengendap = '';
            $scope.bungaperiode = '';
            $scope.sukubungaA = '';
            $scope.saldoterkaitA = '';
            $scope.sukubungaB = '';
            $scope.saldoterkaitB = '';
            $scope.sukubungaC = '';
            $scope.saldoterkaitC = '';
            $scope.sukubungaD = '';
            $scope.saldoterkaitD = '';
        }

        function applyAllModels(param){
            var p = [2, 30, 1, 1, 4, 1, 12, 12, 1, 4, 15, 4, 15, 4, 15, 4, 15];
            var q = ["koderencanabunga", "namarencanabunga", "perhitunganbunga", "tandapajak","persentasepajak", "perhitunganpajak", "minimalpajak", "minimalmengendap", "bungaperiode", "sukubungaA", 
            "saldoterkaitA", "sukubungaB", "saldoterkaitB", "sukubungaC", "saldoterkaitC", "sukubungaD", "saldoterkaitD"];

            Utility.parseStringToModels($scope, q, p, param);
        }

        $scope.bungaClicked = function(param) {
            $scope.selectedBungaIndex = param;
            $scope.selectedBunga = $scope.bungaObjectList[param];
            $scope.selectedBungaEdited = JSON.parse(JSON.stringify($scope.selectedBunga));
            console.log($scope.selectedBungaEdited);
            applyAllModels($scope.selectedBungaEdited.xdata);
        }
        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Data";
            $scope.selectedBunga = {};
            $scope.selectedBungaIndex = -1;
            resetAllModels();
            $scope.menuModal = true;
        }
        $scope.hapusClicked = function() {
            if ($scope.selectedBungaIndex!=-1) {
                var selobj = $scope.bungaList[$scope.selectedBungaIndex];
                var conf = confirm("Apakah Anda akan menghapus data " + selobj.namabunga  + "?");
                var xkunci = "201" + selobj.kode;
                if (conf) {
                   $scope.isLoading=true;
                   $http({
                       url: '/ajax/dspp001',
                       method: 'POST',
                       data: angular.toJson({xkunci: xkunci}),
                       header: {'Content-Type': 'application/json'},
                   }).success(function(data, status, headers, config){
                       console.log(data);
                       $scope.isLoading=false;
                       resetAllModels();
                       fetchBungaList();
                       $scope.selectedBungaIndex=-1;
                       Notification("Data berhasil dihapus!");
                   }).error(function(data, status, headers, config){
                       $scope.isLoading=false;
                       console.log('ERROR');
                   });
                }
            }
        
        }
        $scope.editClicked = function() {
            if ($scope.selectedBungaIndex==-1) {
                return;
            }
            $scope.modalTitle = "Edit Data";
            $scope.selectedBunga = {};
            $scope.selectedBungaIndex = -1;
            $scope.menuModal = true;
        }
        $scope.cancel = function() {
            console.log('NO');
            $scope.menuModal = false;
            resetAllModels();
            fetchBungaList();
            $scope.selectedBungaIndex=-1;
        }

        $scope.simpanButtonClicked = function() {
            if ($scope.koderencanabunga.length != 2) {
                alert('Invalid Kode Rencana Bunga');
                return;
            }
            var finstr = '';
            finstr = finstr + Utility.normalizeCharNum(2, $scope.koderencanabunga);
            finstr = finstr + Utility.normalizeCharNum(30, $scope.namarencanabunga);
            finstr = finstr + Utility.normalizeCharNum(1, $scope.perhitunganbunga);
            finstr = finstr + Utility.normalizeCharNum(1, $scope.tandapajak);
            finstr = finstr + Utility.normalizeCharNum(4, $scope.persentasepajak);
            finstr = finstr + Utility.normalizeCharNum(1, $scope.perhitunganpajak);
            finstr = finstr + Utility.normalizeCharNum(12, $scope.minimalpajak);
            finstr = finstr + Utility.normalizeCharNum(12, $scope.minimalmengendap);
            finstr = finstr + Utility.normalizeCharNum(1, $scope.bungaperiode);

            finstr = finstr + Utility.normalizeCharNum(4, $scope.sukubungaA);
            finstr = finstr + Utility.normalizeCharNum(15, $scope.saldoterkaitA);
            finstr = finstr + Utility.normalizeCharNum(4, $scope.sukubungaB);
            finstr = finstr + Utility.normalizeCharNum(15, $scope.saldoterkaitB);
            finstr = finstr + Utility.normalizeCharNum(4, $scope.sukubungaC);
            finstr = finstr + Utility.normalizeCharNum(15, $scope.saldoterkaitC);
            finstr = finstr + Utility.normalizeCharNum(4, $scope.sukubungaD);
            finstr = finstr + Utility.normalizeCharNum(15, $scope.saldoterkaitD);

            console.log(finstr);

            var data1 = {xkunci: '201' + $scope.koderencanabunga, xdata:finstr};
            var postdata = {postdata:[data1]};
            $scope.isLoading=true;
            $http({ url: '/ajax/cspp001',
                  method: 'POST',
                  data: angular.toJson(postdata),
                  header: {'Content-Type': 'application/json'},
           }).success(function(data, status, headers, config){
               console.log(data);
               $scope.isLoading=false;
               if (data.data) {
                   $scope.menuModal = false;
                   resetAllModels();
                   fetchBungaList();
                   $scope.selectedBungaIndex=-1;
                   Notification("Data berhasil disimpan!"); 
               }
           }).error(function(data, status, headers, config){
                $scope.isLoading=false;
               console.log('ERROR');
           });
            
        }
    });
}(angular.module('ksp')));
