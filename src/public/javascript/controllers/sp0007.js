(function(app) {
    app.controller('sp0007Ctrl', function($scope, $http, $window, $rootScope, Notification, Utility){
        $scope.selectedLayananIndex=-1;
        $scope.isLoading = false;
        $scope.layananList = [];
        $scope.layananObjectList = [];
        $scope.selectedLayanan = {};
        $scope.selectedLayananEdited = {};

        function fetchLayananList() {
            $scope.layananList = [];
            $scope.layananObjectList = [];
            $scope.selectedLayanan = {};
            $scope.selectedLayananEdited = {};
            $http.get('/ajax/rspp001B/301')
            .success(function(response){
                angular.forEach(response.data, function(v,k){
                    var streamchar='';
                    var tmp = {};
                    for (var i=0;i<32;i++) {
                        streamchar=streamchar+v.xdata[i];
                        if (i==1) {
                            tmp.kode = streamchar;
                            streamchar='';
                        }
                    }
                    tmp.namalayanan = streamchar;
                    $scope.layananList.push(tmp);
                    $scope.layananObjectList.push(v);
                })
            });
        }

        $scope.cancel = function() {
            $scope.menuModal = false;
            $scope.selectedLayananIndex=-1;
        }
        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Data";
            $scope.selectedLayanan = {};
            $scope.selectedLayananIndex = -1;
            resetAllModels();
            $scope.menuModal = true;
        }
        $scope.editClicked = function() {
            if ($scope.selectedLayananIndex==-1) {
                return;
            }
            $scope.modalTitle = "Edit Data";
            $scope.selectedLayanan = {};
            $scope.selectedLayananIndex = -1;
            $scope.menuModal = true;
        }

        fetchLayananList();

        function resetAllModels() {
            $scope.kodebiayalayanan='';
            $scope.namabiayalayanan='';
            $scope.biayatransaksidr='';
            $scope.biayatransaksicr='';
            $scope.biayaadministrasi='';
            $scope.biayarekeningpasif='';
        }

        function applyAllModels(param){
            var p = [2, 30, 5, 5, 24, 5];
            var q = ["kodebiayalayanan", "namabiayalayanan", "biayatransaksidr", "biayatransaksicr", "biayaadministrasi", "biayarekeningpasif"];

            Utility.parseStringToModels($scope, q, p, param);
        }
        
        $scope.layananClicked = function(param) {
            $scope.selectedLayananIndex = param;
            $scope.selectedLayanan = $scope.layananObjectList[param];
            $scope.selectedLayananEdited = JSON.parse(JSON.stringify($scope.selectedLayanan));
            console.log($scope.selectedLayananEdited);
            applyAllModels($scope.selectedLayananEdited.xdata);
        }
        $scope.hapusClicked = function() {
            if ($scope.selectedLayananIndex!=-1) {
                var selobj = $scope.layananList[$scope.selectedLayananIndex];
                var conf = confirm("Apakah Anda akan menghapus data " + selobj.namalayanan  + "?");
                var xkunci = "301" + selobj.kode;
                if (conf) {
                   $scope.isLoading=true;
                   $http({
                       url: '/ajax/dspp001',
                       method: 'POST',
                       data: angular.toJson({xkunci: xkunci}),
                       header: {'Content-Type': 'application/json'},
                   }).success(function(data, status, headers, config){
                       console.log(data);
                       $scope.isLoading=false;
                       resetAllModels();
                       fetchLayananList();
                       $scope.selectedLayananIndex=-1;
                       Notification("Data berhasil dihapus!");
                   }).error(function(data, status, headers, config){
                       $scope.isLoading=false;
                       console.log('ERROR');
                   });
                }
            }
        
        }

        $scope.simpanButtonClicked = function() {
            if ($scope.kodebiayalayanan.length != 2) {
                alert('Invalid Kode Biaya Layanan');
                return;
            }
            var finstr = '';
            finstr = finstr + Utility.normalizeCharNum(2, $scope.kodebiayalayanan);
            finstr = finstr + Utility.normalizeCharNum(30, $scope.namabiayalayanan);
            finstr = finstr + Utility.normalizeCharNum(5, $scope.biayatransaksidr);
            finstr = finstr + Utility.normalizeCharNum(5, $scope.biayatransaksicr);
            finstr = finstr + Utility.normalizeCharNum(24, $scope.biayaadministrasi);
            finstr = finstr + Utility.normalizeCharNum(5, $scope.biayarekeningpasif);

            console.log(finstr);

            var data1 = {xkunci: '301' + $scope.kodebiayalayanan, xdata:finstr};
            var postdata = {postdata:[data1]};
            $scope.isLoading=true;
            $http({ url: '/ajax/cspp001',
                  method: 'POST',
                  data: angular.toJson(postdata),
                  header: {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
               console.log(data);
               $scope.isLoading=false;
               if (data.data) {
                   $scope.menuModal = false;
                   resetAllModels();
                   fetchLayananList ();
                   $scope.selectedLayananIndex=-1;
                   Notification("Data berhasil disimpan!"); 
               }
            }).error(function(data, status, headers, config){
                $scope.isLoading=false;
               console.log('ERROR');
            });
        }
    });
}(angular.module('ksp')));
