(function(app) {
    app.controller('sp0012Ctrl', function($scope, $http, $window, $rootScope, Notification, Utility){
        $scope.isLoading=false;
        $scope.activeTab=1;
        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Data";
            $scope.menuModal = true;
        }
        $scope.cancel = function() {
            $scope.menuModal = false;
        }
        $scope.setActiveTab = function(param) {
            $scope.activeTab=param;
        }
    });
}(angular.module('ksp')));
