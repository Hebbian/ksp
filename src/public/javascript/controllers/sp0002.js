(function(app) {
    app.controller('sp0002Ctrl', function($scope, $http, $window, $rootScope, Notification){
        function stringParser(count, string) {
            var ret=[], tmp='', counter=0;
            for (var i=0; i< string.length; i++) {
                tmp+=string[i];
                if (counter==(count-1)) {
                    ret.push(tmp);
                    tmp='';
                    counter=0;
                } else {
                    counter++;
                }
            } 
            return ret;
        }
        $scope.$watch('viewdata', function(param){
            $scope.isLoading=false;
            var p = [35, 40, 40, 40, 5, 12, 12, 25, 12 ,20];
            var q = ["namainstansibpr", "alamatA", "alamatB", "alamatC","kodepos", "telepon", "fax", "npwp", "pelapor", "kecamatan" ];
            var streamchar = "";
            var counter=0;
            var elem=0;
            var elemval=p[elem];
            for (var i=0; i< param.length; i++) {
                streamchar = streamchar + param[i];
                counter = i+1; 
                if (counter==elemval) {
                    $scope[q[elem]] = streamchar;
                    elem+=1;
                    elemval+=p[elem];
                    console.log(streamchar);
                    streamchar="";
                }
            }
        });

        function normalizeCharNum(charnum, charstr) {
            var tmp = charstr;
            for (var i=0;i<charnum;i++) {
                if (!tmp[i]) {
                    tmp=tmp+" ";
                }
            }
            return tmp;
            // str = str + new Array(charnum + 1).join(' ');
        }

        $scope.identitasSaveButtonDidClicked = function() {
            var data1 = {xkunci:'000', xdata: normalizeCharNum(35, $scope.namainstansibpr) + normalizeCharNum(40, $scope.alamatA) +
                normalizeCharNum(40, $scope.alamatB) + normalizeCharNum(40, $scope.alamatC) + normalizeCharNum(5, $scope.kodepos) +
                    normalizeCharNum(12, $scope.telepon) + normalizeCharNum(12, $scope.fax) + normalizeCharNum(25, $scope.npwp) + normalizeCharNum(12, $scope.pelapor) + normalizeCharNum(20, $scope.kecamatan)}; 
            var postdata = {postdata:[data1]};
            $scope.isLoading=true;
            $http({ url: '/ajax/cspp001',
                  method: 'POST',
                  data: angular.toJson(postdata),
                  header: {'Content-Type': 'application/json'},
           }).success(function(data, status, headers, config){
               console.log(data);
               $scope.isLoading=false;
               if (data.data) {
                   Notification("Data berhasil disimpan!"); }
           }).error(function(data, status, headers, config){
                $scope.isLoading=false;
               console.log('ERROR');
           });
        }
    });
}(angular.module('ksp')));
