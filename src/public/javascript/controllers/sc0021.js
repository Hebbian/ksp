(function(app) {
    app.controller('subMenuLevel2Ctrl', function($scope, $http, Notification){
        $scope.isLoading=false;
        $scope.selectedMenu='';
        $scope.selectedMenuID=-1;
        $scope.selectedSubmenu={};
        $scope.selectedSubmenuEdited={};
        $scope.selectedSubmenuIndex=-1;
        $scope.submenus=[];
        $scope.newsubmenu={};
        $scope.$watch('selectedSubmenu', function(param){
        });
        $scope.$watch('selectedMenu', function(param){
            if (param.length > 0) {
                $http.get('/ajax/scp011/'+param)
                .success(function(response){
                    $scope.submenus = response.data.scp011s || [];
                    $scope.selectedSubmenuIndex=-1;
                    $scope.selectedMenuID = response.data.sdaplnum;
                    console.log($scope.selectedMenuID);
                });
            }
        });
        $scope.menuClicked = function(param) {
        }
        $scope.submenuClicked = function(param) {
            $scope.selectedSubmenu = $scope.submenus[param];
            $scope.selectedSubmenuIndex = param;
        }
        $scope.hapusSubmenu = function() {
            if ($scope.selectedSubmenuIndex!=-1) {
                var conf = confirm("Apakah Anda akan menghapus menu " + $scope.selectedSubmenu.scodsc + "?");
                if (conf) {
                    $scope.isLoading=true;
                   $http({
                       url: '/ajax/dscp011',
                       method: 'POST',
                       data: angular.toJson($scope.selectedSubmenu),
                       header: {'Content-Type': 'application/json'},
                   }).success(function(data, status, headers, config){
                       console.log(data);
                        $scope.isLoading=false;
                       if (data.data) {
                           $scope.submenus.splice($scope.selectedSubmenuIndex, 1);
                           $scope.selectedeSubmenu={};
                           $scope.selectedSubmenuIndex=-1;
                           Notification("Data berhasil dihapus!"); }
                   }).error(function(data, status, headers, config){
                        $scope.isLoading=false;
                       console.log('ERROR');
                   });
                }
            }
        }

        $scope.open = function() {
            console.log($scope.selectedMenuID);
            if ($scope.selectedMenuID!=-1) {
                $scope.showModal = true;
            }
        };
        $scope.ok = function() {
            $scope.showModal = false;
        }
        $scope.cancel = function() {
            $scope.showModal = false;
        }
        $scope.submitNewSubmenu = function(isValid) {
            if (!isValid) {
                alert('Invalid data');
            } else {
                $scope.newsubmenu.scp012Sdaplnum = $scope.selectedMenuID;
                console.log($scope.newsubmenu); 
                $scope.isLoading=true;
                $http({
                    url: '/ajax/cscp011',
                    method: 'POST',
                    data: angular.toJson($scope.newsubmenu),
                    header: {'Content-Type': 'application/json'},
                }).success(function(data, status, headers, config){
                    if (data.data) {
                        $scope.showModal = false;
                        $scope.submenus.push(data.data);
                        $scope.newsubmenu={};
                        $scope.isLoading=false;
                        Notification("Menu berhasil ditambahkan");
                    }
                }).error(function(data, status, headers, config){
                        $scope.isLoading=false;
                });
            }
        };
        $scope.openEdit = function() {
            if ($.isEmptyObject($scope.selectedSubmenu)) {
            
            } else {
                $scope.selectedSubmenuEdited = JSON.parse(JSON.stringify($scope.selectedSubmenu));
                $scope.editModal = true;
            }
        }
        $scope.editModalClose = function() {
            $scope.editModal = false;
        }
        $scope.submitEditSubmenu = function(isValid){
            console.log($scope.selectedSubmenuEdited);
            if (!isValid) {
                alert('Invalid data');
            } else {
                $scope.isLoading=true;
                $http({
                    url: '/ajax/uscp011',
                    method: 'POST',
                    data: angular.toJson($scope.selectedSubmenuEdited),
                    header: {'Content-Type': 'application/json'},
                }).success(function(data, status, headers, config){
                    console.log(status);
                    if (data.data) {
                        $scope.submenus[$scope.selectedSubmenuIndex] = JSON.parse(JSON.stringify($scope.selectedSubmenuEdited));
                        $scope.selectedSubmenu = $scope.submenus[$scope.selectedSubmenuIndex];
                        Notification("Data berhasil diubah!");
                    }
                    $scope.editModal = false;
                    $scope.isLoading=false;
                }).error(function(data, status, headers, config){
                    console.log('ERROR');
                    console.log(status);
                    $scope.isLoading=false;
                });
            }
        }
        $scope.editSelect  = function(param1,param2) {
            return param1==param2;
        }
        $scope.scttypText = function(param){
            if (paramm==1) return "Inquiry";
            else if (paramm==2) return "Maintenance";
            else if (paramm==3) return "Monetary";
            else if (paramm==4) return "Operatory";
            else return param;
        }
    });
}(angular.module('ksp')));
