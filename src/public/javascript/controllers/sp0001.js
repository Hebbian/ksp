(function(app) {
    app.controller("parameterAplikasiTanggalCtrl", function($scope, $http, $window, Notification){
        $scope.isLoading = false;
        $scope.application_code_text = "Input Transaksi";
        $scope.application_code=10;
        fetchByAppCode($scope.application_code);
        

        $http({
            url: '/ajax/rspp001/006',
            method: 'GET',
            data: angular.toJson(),
            header:  {'Content-Type': 'application/json'},
        }).success(function(data, status, headers, config){
            var a = stringParser(10, data.data.xdata);
            $scope.dateF = a[0];
            $scope.dateE = a[1];
        }).error(function(data, status, headers, config){

        });

        $http({
            url: '/ajax/rspp001/007',
            method: 'GET',
            data: angular.toJson(),
            header:  {'Content-Type': 'application/json'},
        }).success(function(data, status, headers, config){
            $scope.dateD = data.data.xdata;
        }).error(function(data, status, headers, config){

        });


        var dateAutoComplete = function (str) {
            str = str.trim();
            var matches, year,
                    looksLike_MM_slash_DD = /^(\d\d\/)?\d\d$/,
                    looksLike_MM_slash_D_slash = /^(\d\d\/)?(\d\/)$/,
                    looksLike_MM_slash_DD_slash_DD = /^(\d\d\/\d\d\/)(\d\d)$/;
     
            if( looksLike_MM_slash_DD.test(str) ){
                    str += "/";
            }else if( looksLike_MM_slash_D_slash.test(str) ){
                    str = str.replace( looksLike_MM_slash_D_slash, "$10$2");
            }else if( looksLike_MM_slash_DD_slash_DD.test(str) ){
                    matches = str.match( looksLike_MM_slash_DD_slash_DD );
                    year = "20"; //Number( matches[2] ) < 20 ? "20" : "19";
                    str = String( matches[1] ) + year + String(matches[2]);
            }
            return str;
        };


        $(".date").keyup(function(e){
            var currentValue = $(e.target).val();
            var newValue = dateAutoComplete( currentValue );
            if( newValue != currentValue ){
                    $(e.target).val( newValue );
            }
        });

        $scope.tab1SaveClicked = function() {
            var finstr='';
            for (var i=1;i<24;i++) {
                var modelname = "tanggal"+i;
                var val = $scope[modelname];
                if (val !== undefined) {
                    var a = moment($scope[modelname], "DDMMYYYY").format("DD/MM/YYYY"); 
                    if (a!=='Invalid date') {
                        finstr+=a;
                    }
                }
            }
            $scope.isLoading = true;
            var data1 = {xkunci:'001', xdata:finstr};
            var postdata = {postdata:[data1]};
            $http({
                url: '/ajax/cspp001',
                method: 'POST',
                data: angular.toJson(postdata),
                header:  {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
                $scope.isLoading = false;
                Notification("Data berhasil disimpan!");
                parseAndApplyAlldates(finstr);
            }).error(function(data, status, headers, config){
                $scope.isLoading = false;
            });
        }

        function parseAndApplyAlldates(param) {
            $scope.alldates = stringParser(10, param);
            for (var i=0;i<$scope.alldates.length;i++) {
                var modelname = "tanggal"+(i+1)
                $scope[modelname] = $scope.alldates[i];
            }
        
        }

        $scope.alldates=[];
        $scope.$watch("dates", function(param){
            parseAndApplyAlldates(param);
        });

        function stringParser(count, string) {
            var ret=[], tmp='', counter=0;
            for (var i=0; i< string.length; i++) {
                tmp+=string[i];
                if (counter==(count-1)) {
                    ret.push(tmp);
                    tmp='';
                    counter=0;
                } else {
                    counter++;
                }
            } 
            return ret;
        }


        //TAB2

        function makeIndonesianDay(param) {
            var dayname = param.toLowerCase();
            if (param==="sunday") {
                return "minggu";
            } else if (dayname==="monday") {
                return "senin";
            } else if (dayname==="tuesday") {
                return "selasa";
            } else if (dayname==="wednesday") {
                return "rabu";
            } else if (dayname==="thursday") {
                return "kamis";
            } else if (dayname==="friday") {
                return "jumat";
            } else if (dayname==="wednesday") {
                return "sabtu";
            }
        }

        function makeDayNumber(param) {
            var dayname = param.toLowerCase();
            if (param==="sunday") {
                return 7;
            } else if (dayname==="monday") {
                return 1;
            } else if (dayname==="tuesday") {
                return 2;
            } else if (dayname==="wednesday") {
                return 3;
            } else if (dayname==="thursday") {
                return 4;
            } else if (dayname==="friday") {
                return 5;
            } else if (dayname==="wednesday") {
                return 6;
            }
        
        }

        function fetchByAppCode(param) {
            var xkunci = '004' + param;
            $http({
                url: '/ajax/gspp001/' + xkunci,
                method: 'GET',
                data: angular.toJson(),
                header:  {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
                console.log(data);
                if (data.data !== null) {
                    var xdatas = data.data.xdata;
                    var splitted = xdatas.split(" ");
                    var xdata = splitted[0];

                    //xdata[0]
                    var dates='';
                    for (var i=0; i<30; i++) {
                        dates+=data.data.xdata[i];
                    }
                    var datesarray = stringParser(10,dates); 
                    $scope.dateA = datesarray[0];
                    $scope.dateB = datesarray[1];
                    $scope.dateC = datesarray[2];
                    var dayname = moment($scope.dateA, "DD/MM/YYYY").format('dddd');
                    $scope.kodeharinum = makeDayNumber(dayname);
                    $scope.dayname = makeIndonesianDay(dayname);
                    $scope.dayname = $scope.dayname.charAt(0).toUpperCase() + $scope.dayname.slice(1);

                    //soo ugly
                    $scope.minggu = (xdata[30]==0) ? false : true;
                    $scope.senin = (xdata[31]==0) ? false : true;
                    $scope.selasa = (xdata[32]==0) ? false : true;
                    $scope.rabu = (xdata[33]==0) ? false : true;
                    $scope.kamis = (xdata[34]==0) ? false : true;
                    $scope.jumat = (xdata[35]==0) ? false : true;
                    $scope.sabtu = (xdata[36]==0) ? false : true;

                    for (var i=30; i<37; i++) {
                        dates+=data.data.xdata[i];
                    }

                    //xdata[1]
                    $scope.rekA='';
                    $scope.rekA1='';
                    $scope.rekB='';
                    $scope.rekB1='';
                    $scope.rekC='';
                    $scope.rekC1='';
                    $scope.rekD='';
                    $scope.rekD1='';
                    var reks = stringParser(10, splitted[1]);
                    for (var i=0; i<reks.length; i++) {
                        var a="rek";
                        var b="A";
                        var varname=a+b;
                        if (i==0) {
                            varname=a+b;
                        } else if (i==1) {
                            b="B";
                            varname=a+b;
                        } else if (i==2) {
                            b="C";
                            varname=a+b;
                        } else if (i==3) {
                            b="D";
                            varname=a+b;
                        }
                        for (var k=0;k<7;k++) {
                            $scope[varname] = $scope[varname] + reks[i][k];
                        }
                        for (var k=7;k<10;k++) {
                            $scope[varname+'1'] = $scope[varname+'1'] + reks[i][k];
                        }
                    }
                } else {
                    $scope.dateA = '';
                    $scope.dateB = '';
                    $scope.dateC = '';
                    $scope.nextdayname = '';
                    $scope.minggu = false;
                    $scope.senin = false;
                    $scope.selasa = false;
                    $scope.rabu = false;
                    $scope.kamis = false;
                    $scope.jumat = false;
                    $scope.sabtu = false;
                }
                
            }).error(function(data, status, headers, config){

            });
        }
        $scope.chg = function(param){
            console.log(param);
            if (param==="10") {
                $scope.application_code_text = "Input Transaksi";
            } else if (param==="20") {
                $scope.application_code_text = "Tabungan";
            } else if (param==="30") {
                $scope.application_code_text = "Deposito";
            } else if (param==="40") {
                $scope.application_code_text = "Buku Besar";
            } else if (param==="50") {
                $scope.application_code_text = "Pinjaman";
            }
            fetchByAppCode($scope.application_code);
        }
        function isDayProcessed(param) {
            if (param==='Sunday') {
                return $scope.minggu;
            }
            if (param==='Monday') {
                return $scope.senin;
            }
            if (param==='Tuesday') {
                return $scope.selasa;
            }
            if (param==='Wednesday') {
                return $scope.rabu;
            }
            if (param==='Thursday') {
                return $scope.kamis;
            }
            if (param==='Friday') {
                return $scope.jumat;
            }
            if (param==='Saturday') {
                return $scope.sabtu;
            }
        }
        // soooo ugly
        function checkProcessedDays() {
            if ($scope.minggu==false && $scope.senin==false && $scope.selasa==false && $scope.rabu==false && $scope.kamis==false && $scope.jumat==false && $scope.sabtu==false) {
                return false;
            }
            return true;
        }
        $scope.dateAChanged = function(param) {
            reloadDateProcess();
        };
        $scope.dayChanged = function(param) {
            reloadDateProcess();
        }
        function reloadDateProcess() {
            if ($scope.dateA !== undefined && $scope.dateA.length>0) {
                if (checkProcessedDays()==false) {
                    alert('Hari proses tidak boleh kosong');
                    $scope.dateA='';
                    $scope.dateB='';
                    $scope.dateC='';
                    return;
                }
                var date = moment($scope.dateA, "DDMMYYYY");
                $scope.dayname = date.format('dddd');

                var curmonth = date.format('MM');
                var nextdayobject = date.add(1,'days');
                var nextday = nextdayobject.format('DD/MM/YYYY');
                var curdayname = nextdayobject.format('dddd');
                var endofmonth = '0';
                var endofmonthalreadyset = false;
                while($scope.alldates.indexOf(nextday) != -1 || isDayProcessed(curdayname)==false) {
                    nextdayobject  = moment(nextday, 'DD/MM/YYYY').add(1,'d');
                    nextday = nextdayobject.format('DD/MM/YYYY');
                    curdayname = nextdayobject.format('dddd');

                    var nextcurmonth = nextdayobject.format('MM');
                    if (nextcurmonth != curmonth && endofmonthalreadyset==false) {
                        endofmonth = moment(date, 'DD/MM/YYYY').subtract(1, 'm').endOf('month').format('DD/MM/YYYY');
                    }
                }
                if (endofmonth!=='0') {
                    $scope.dateB = endofmonth;
                    $scope.dateC = nextday;
                } else {
                    $scope.dateB = moment(nextday, "DD/MM/YYYY").subtract(1,'d').format('DD/MM/YYYY');
                    $scope.dateC = nextday;
                }
            } else {
                $scope.dateB='';
                $scope.dateC='';
            }
        }
        function dayhelper(param) {
            if (param==undefined || param==false) {
                return 0;
            } else {
                return 1;
            }
        }
        function normalizeDate(param) {
            return moment(param, "DDMMYYYY").format("DD/MM/YYYY");
        }
        $scope.tab2SaveClicked = function() {
            var dateA2 = moment($scope.dateA, "DDMMYYYY").format("DD/MM/YYYY");
            // sooo ugly
            var tab2finstr = dateA2 + $scope.dateB + normalizeDate($scope.dateC) + dayhelper($scope.minggu) + dayhelper($scope.senin) + dayhelper($scope.selasa) + dayhelper($scope.rabu) + dayhelper($scope.kamis) + dayhelper($scope.jumat) + dayhelper($scope.sabtu);
            var tab2finstr = tab2finstr + " " + $scope.rekA + $scope.rekA1 + $scope.rekB + $scope.rekB1 + $scope.rekC + $scope.rekC1 + $scope.rekD + $scope.rekD1;
            var keyname = '004'+$scope.application_code;

            /*
             * structure: 
             * {
             *  postdata: [
             *      {
             *          xkunci: ...
             *          xdata: ...
             *      },
             *      {
             *          xkunci: ...
             *          xdata: ...
             *      }
             *  ]
             * }
             */

            var data1 = {xkunci: keyname, xdata: tab2finstr};
            var data2 = {xkunci: '007', xdata: normalizeDate($scope.dateD)};
            var data3 = {xkunci: '006', xdata: normalizeDate($scope.dateF) + normalizeDate($scope.dateE)};
            var postdata = {postdata: [data1, data2, data3]};
            console.log(postdata);

            $scope.isLoading = true;
            $http({
                url: '/ajax/cspp001',
                method: 'POST',
                data: angular.toJson(postdata),
                header:  {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
                $scope.isLoading = false;
                Notification("Data berhasil disimpan!");
            }).error(function(data, status, headers, config){
                $scope.isLoading = false;
            });
        }

    });
}(angular.module('ksp')));
