(function(app) {
    app.controller('sp0008Ctrl', function($scope, $http, $window, $rootScope, Notification, Utility){
        $scope.isLoading=false;
        $scope.activeTab=1;
        $scope.selectedProdukIndex=-1;
        $scope.produkList = [];
        $scope.produkObjectList = [];
        $scope.selectedProduk = {};
        $scope.selectedProdukEdited = {};
        var prefixdb = "203";
        var arraynum = [2, 30, 1, 1, 1, 10, 10, 10, 1, 3, 2, 2, 1, 1, 1, 10, 10, 1, 2, 2, 2, 1, 3, 3, 3, 1, 1];


        function fetchProdukList() {
            $scope.produkList = [];
            $scope.produkObjectList = [];
            $scope.selectedProduk = {};
            $scope.selectedProdukEdited = {};
            $http.get('/ajax/rspp001B/' + prefixdb)
            .success(function(response){
                angular.forEach(response.data, function(v,k){
                    var streamchar='';
                    var tmp = {};
                    for (var i=0;i<32;i++) {
                        streamchar=streamchar+v.xdata[i];
                        if (i==1) {
                            tmp.kode = streamchar;
                            streamchar='';
                        }
                    }
                    tmp.nama = streamchar;
                    $scope.produkList.push(tmp);
                    $scope.produkObjectList.push(v);
                })
            });
        }

        fetchProdukList();

        $scope.tambahClicked = function() {
            $scope.modalTitle = "Tambah Data";
            $scope.menuModal = true;
            $scope.selectedProduk={};
            $scope.selectedProdukIndex=-1;
            resetAllModels();
        }
        function resetAllModels(){
            for (var i=0;i<arraynum.length;i++) {
                $scope["model"+(i+1)] = '';
            }
        }
        $scope.cancel = function() {
            $scope.menuModal = false;
        }
        $scope.setActiveTab = function(param) {
            $scope.activeTab=param;
        }
        function applyAllModels(param){
            var q=[];
            for (var i=0;i<arraynum.length;i++) {
                q.push("model"+(i+1));
            }
            Utility.parseStringToModels($scope, q, arraynum, param);
        }
        $scope.produkClicked = function(param) {
            $scope.selectedProdukIndex = param;
            $scope.selectedProduk = $scope.produkObjectList[param];
            $scope.selectedProdukEdited = JSON.parse(JSON.stringify($scope.selectedProduk));
            applyAllModels($scope.selectedProdukEdited.xdata);
        }
        $scope.editClicked = function() {
            if ($scope.selectedProdukIndex==-1) {
                return;
            }
            $scope.modalTitle = "Edit Data";
            $scope.selectedProduk = {};
            $scope.selectedProdukIndex= -1;
            $scope.menuModal = true;
        }

        $scope.simpanButtonClicked = function() {
            var finstr='';
            for (var i=0; i<arraynum.length; i++){
                var modelname = "model"+(i+1);
                var normalize = Utility.normalizeCharNum(arraynum[i], $scope[modelname]);
                finstr = finstr+normalize;

                console.log("model"+(i+1) + ": " + normalize);
            }
            console.log(finstr);
            var data1 = {xkunci: prefixdb + $scope["model1"], xdata:finstr};
            var postdata = {postdata:[data1]};
            $scope.isLoading=true;
            $http({ url: '/ajax/cspp001',
                  method: 'POST',
                  data: angular.toJson(postdata),
                  header: {'Content-Type': 'application/json'},
            }).success(function(data, status, headers, config){
               console.log(data);
               $scope.isLoading=false;
               if (data.data) {
                   $scope.menuModal = false;
                   $scope.selectedProdukIndex=-1;
                   fetchProdukList();
                   Notification("Data berhasil disimpan!"); 
               }
            }).error(function(data, status, headers, config){
                $scope.isLoading=false;
                console.log('ERROR');
            });
        }
        $scope.hapusClicked = function() {
            if ($scope.selectedProdukIndex!=-1) {
                var selobj = $scope.produkList[$scope.selectedProdukIndex];
                var conf = confirm("Apakah Anda akan menghapus data " + selobj.nama  + "?");
                var xkunci = prefixdb + selobj.kode;
                if (conf) {
                   $scope.isLoading=true;
                   $http({
                       url: '/ajax/dspp001',
                       method: 'POST',
                       data: angular.toJson({xkunci: xkunci}),
                       header: {'Content-Type': 'application/json'},
                   }).success(function(data, status, headers, config){
                       console.log(data);
                       $scope.isLoading=false;
                       resetAllModels();
                       fetchProdukList();
                       $scope.selectedProdukIndex =-1;
                       Notification("Data berhasil dihapus!");
                   }).error(function(data, status, headers, config){
                       $scope.isLoading=false;
                       console.log('ERROR');
                   });
                }
            }
        }
    });
}(angular.module('ksp')));
