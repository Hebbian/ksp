INSERT INTO "scp001s" ("scuser","scunme","scpass","scstat") VALUES ('MANAGER1','Manager 1','$2a$10$9NjjLPuHCgFq0mZyOl95gOzqroiqzE2MpNieuqbPiV0yU80Hf442W',true);
INSERT INTO "scp012s" ("sdaplnum","sdapldsc") VALUES (DEFAULT,'Pengawasan - Security');
INSERT INTO "scp012s" ("sdaplnum","sdapldsc") VALUES (DEFAULT,'Parameter Aplikasi');
INSERT INTO "scp002s" ("id","sauser","saappl","sainqu","samain","samoni","saoper","sadrlm","sacrlm","saover") VALUES (DEFAULT,'MANAGER1','1',9,9,9,9,0,0,false);
INSERT INTO "scp011s" ("id","sctran","scpgm","scttyp","sctlvl","sctovl","scdlvl","scodsc","scp012Sdaplnum") VALUES (DEFAULT,'N','sc0001',2,2,0,0,'Pemeliharaan Profil Pemakai',1);
INSERT INTO "scp011s" ("id","sctran","scpgm","scttyp","sctlvl","sctovl","scdlvl","scodsc","scp012Sdaplnum") VALUES (DEFAULT,'N','sc0020',2,2,0,0,'Pemeliharaan Menu Level 1',1);
INSERT INTO "scp011s" ("id","sctran","scpgm","scttyp","sctlvl","sctovl","scdlvl","scodsc","scp012Sdaplnum") VALUES (DEFAULT,'N','sc0021',2,2,0,0,'Pemeliharaan Menu Level 2',1);
INSERT INTO "scp011s" ("id","sctran","scpgm","scttyp","sctlvl","sctovl","scdlvl","scodsc","scp012Sdaplnum") VALUES (DEFAULT,'N','pa001',2,2,0,0,'Parameter Tanggal',2);
